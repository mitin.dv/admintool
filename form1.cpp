#include "form1.h"
#include "ui_form1.h"

Form1::Form1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form1)
{
    ui->setupUi(this);
}

Form1::~Form1()
{
    delete ui;
}

void Form1::on_lineEdit_textEdited(const QString &arg1)
{

}

void Form1::on_lineEdit_textChanged(const QString &arg1)
{

}


void Form1::on_pushButton_clicked()
{
    emit s_1(ui->lineEdit->text());
    emit s_2(ui->lineEdit_2->text());
    emit s_3(ui->lineEdit_3->text());
    emit s_4(ui->lineEdit_4->text());
    hide();

}

void Form1::on_pushButton_2_clicked()
{
    hide();
}
