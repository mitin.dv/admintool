#ifndef NSLOOJUPFORM_H
#define NSLOOJUPFORM_H

#include <QWidget>

namespace Ui {
class nsloojupform;
}

class Nsloojupform : public QWidget
{
    Q_OBJECT

public:
    explicit Nsloojupform(QWidget *parent = 0);
    ~Nsloojupform();

private:
    Ui::nsloojupform *ui;
};

#endif // NSLOOJUPFORM_H
