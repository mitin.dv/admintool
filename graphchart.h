#ifndef GRAPHCHART_H
#define GRAPHCHART_H

#include <QWidget>
#include <ui_graphchart.h>

#include <QtCharts>
#include <QHBoxLayout>

namespace Ui {
class graphchart;
}

class Graphchart : public QWidget
{
    Q_OBJECT


private:
    QChartView * chartView;


public:
    explicit Graphchart(QWidget *parent = 0);
    ~Graphchart();

private:
    Ui::graphchart *ui;


public slots:
    void setPie(int Count_1, int Count_0 );


private:
    QHBoxLayout* hlay;
    QChart* chart;

    QPieSeries* pie;


//    QValueAxis* xAxis;
//    QValueAxis* yAxis;
    int count_1=0, count_0=0;

};

#endif // GRAPHCHART_H
