#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMessageBox>
#include <form1.h>
#include <ping.h>
#include <nsloojupform.h>
#include <QTimer>


const float t_period = 0.05;


namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    Form1* form1;
    Ping* ping;
    Nsloojupform* nsloojupform;
    QTimer* timer;

private slots:
    void timer_resalt();
    void on_pushButton_2_clicked();
    void odqqdd();
    void pingbutton();

    void on_comboBox_currentIndexChanged(int index);

    void on_groupBox_2_toggled(bool arg1);

    void on_pushButton_8_toggled(bool checked);



    void on_pushButton_7_toggled(bool checked);



    void on_pushButton_10_toggled(bool checked);

private:
    Ui::Widget *ui;

    int count_1 = 0, count_0 =0;
};

#endif // WIDGET_H
