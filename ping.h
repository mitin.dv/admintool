#ifndef PING_H
#define PING_H

#include <QWidget>

namespace Ui {
class Ping;
}

class Ping : public QWidget
{
    Q_OBJECT

public:
    explicit Ping(QWidget *parent = 0);
    ~Ping();

private slots:
    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Ping *ui;
};

#endif // PING_H
