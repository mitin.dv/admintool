#include "widget.h"
#include "ui_widget.h"
#include "ping.h"
#include "random"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    form1 = new Form1;
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(odqqdd()));
    ping = new Ping;

    timer = new QTimer;
    timer->start(t_period*1000);

    connect (timer,SIGNAL(timeout()),this,SLOT(timer_resalt()));
    connect(ui->pushButton_7,SIGNAL(toggled(bool)),this,SLOT(pingbutton()));
    connect(form1,SIGNAL(s_1(QString)),ui->label_4,SLOT(setText(QString)));
    connect(form1,SIGNAL(s_2(QString)),ui->label_6,SLOT(setText(QString)));
    connect(form1,SIGNAL(s_3(QString)),ui->label_10,SLOT(setText(QString)));
    connect(form1,SIGNAL(s_4(QString)),ui->label_8,SLOT(setText(QString)));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::timer_resalt()
{
    static QString str = "";
    static int ones_count = 0;

    if (ones_count<7)
    {
        str+="1";
        count_1++;
        ones_count++;
    }
    else
    {
        if (ones_count<10)
        {
            int r = rand()%2;
            if (r)

            {str+="1";
                count_1++;
            }
            else
            {
                str+="0";
                count_0++;
            }

            ones_count++;
        }
        else
        {
            ones_count = 0;
        }

    }



    ui->pie->setPie(count_1,count_0);


    ui->textEdit_2->setText(str);
    ui->textEdit_2->moveCursor(QTextCursor::End);


    ui->label_14->setText(QString::number(count_1)+"\\"+QString::number(count_0));


}

void Widget::on_pushButton_2_clicked()
{
    form1->show();


}

void Widget::odqqdd()
{
    QMessageBox::warning(this,"Предупреждение","Здесь должно было быть системное окно для изменения настроек сетевого адаптера");



}

void Widget::pingbutton()
{

}

void Widget::on_comboBox_currentIndexChanged(int index)
{
    if (index == 1)
        ui->label_3->setText("SM4-1");

    if (index == 0)
    {
        ui->label_3->setText("SM4-3");

    }
}

void Widget::on_groupBox_2_toggled(bool arg1)
{
    ping->show();
}

void Widget::on_pushButton_8_toggled(bool checked)
{
    nsloojupform->show();
}


void Widget::on_pushButton_7_toggled(bool checked)
{
    if (checked)
        ping->show();
    else
        ping->hide();

    ping->setGeometry(width()+8,0,ping->width(),ping->height());
}



void Widget::on_pushButton_10_toggled(bool checked)
{
    if (checked)
    {

        ui->pie->setPie(count_1,count_0);
    }

    else
    {

    }


}
