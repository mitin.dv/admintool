#include "graphchart.h"
#include "ui_graphchart.h"

Graphchart::Graphchart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::graphchart)
{
    ui->setupUi(this);



    pie = new QPieSeries;
//    xAxis = new QValueAxis;
//    yAxis = new QValueAxis;
    chart = new QChart;
    chart->addSeries(pie);


//    chart->setAxisX(xAxis,splineSeries);
//    chart->setAxisY(yAxis,splineSeries);
//    chart->setAxisX(xAxis,scatterSeries);
//    chart->setAxisY(yAxis,scatterSeries);

    chartView = new QChartView(this);

    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    hlay->setSpacing(0);
    setLayout(hlay);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setChart(chart);

}

Graphchart::~Graphchart()
{
    delete ui;
}

void Graphchart::setPie(int Count_1, int Count_0)
{
    count_0 = Count_0;
    count_1 = Count_1;
    pie->clear();
    pie->append("Обрыв",count_0);
    pie->append("Корректно",count_1);

}

